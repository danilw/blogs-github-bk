### This is copy/backup of my blog [https://arugl.medium.com/](https://arugl.medium.com/)
___

#### List of blog posts:

- [Into Shadertoy and Shaders useful links and tips](Into_Shadertoy_and_Shaders_useful_links_and_tips/README.md)
- [Games in the GPU shaders](Games_in_the_GPU_shaders/README.md)
- [Simple 2D SDF tutorial](simple_SDF_tutorial/README.md)
- [Launching 619 thousand Tetris on GPU, their rendering, and a simple bot](Launching_619_thousand_Tetris_on_GPU/README.md)
- [Decompiling and optimizing Nvidia shaders](decompiling_and_optimizing_nvidia_shaders/README.md)
